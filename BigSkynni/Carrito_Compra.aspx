﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Carrito_Compra.aspx.cs" Inherits="BigSkynni.Carrito_Compra" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            
            <div class="col-md-3">
                <p class="lead">Opciones</p>
                <div class="list-group">
                    <a href="/Busqueda.aspx" class="list-group-item">Búsqueda de Productos</a>
                    <a href="/Carrito_Compra.aspx" class="list-group-item">Carrito de Compra</a>
                    <a href="/Pedido_Venta.aspx" class="list-group-item">Compras Realizadas</a>
                </div>
            </div>
        <div class="col-md-9">
                <div>
                        <h1><u>Carrito de Compra </u></h1>
                        <br>  

                        
                        <asp:GridView ID="GridView1" CssClass ="table table-responsive" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id_carritoCompra" DataSourceID="DS_Carrito" Width="780px">
                            <Columns>
                                <asp:CommandField ShowDeleteButton="True" />
                                <asp:BoundField DataField="nombre" HeaderText="Nombre" SortExpression="nombre" />
                                <asp:BoundField DataField="cantidad" HeaderText="Cantidad" SortExpression="cantidad" />
                                <asp:BoundField DataField="precio" HeaderText="Precio" SortExpression="precio" FooterText="total" />
                                <asp:BoundField DataField="total" HeaderText="Total" SortExpression="total" />
                                <asp:ImageField DataImageUrlField="url_foto">
                                    <ControlStyle Height="60px" Width="60px" />
                                </asp:ImageField>
                            </Columns>
                              <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DS_Carrito" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" DeleteCommand="DELETE FROM Carrito_Compra FROM Carrito_Compra INNER JOIN Producto ON Carrito_Compra.id_producto = Producto.id_producto WHERE (Carrito_Compra.id_carritoCompra = @id_carritoCompra)" SelectCommand="SELECT Producto.precio, Producto.nombre, Producto.url_foto, Carrito_Compra.id_carritoCompra, Carrito_Compra.id_producto, Carrito_Compra.cantidad, Carrito_Compra.id_cliente, (Producto.precio * Carrito_Compra.cantidad) as total FROM Carrito_Compra INNER JOIN Producto ON Carrito_Compra.id_producto = Producto.id_producto WHERE (Carrito_Compra.id_cliente = @id_cliente)">
                            <DeleteParameters>
                                <asp:Parameter Name="id_carritoCompra" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:Parameter Name="id_cliente" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        </div>
            </div>
            </div>
        <div>
            <div class="col-md-3">
                <p> <br /> </p>
                <div >
                    <br />
                    <br />
                    <br />
                </div>

            </div>
                        <br />
            <div class="col-md-9">
                    <div>

                            <asp:GridView CssClass="table table-responsive" ID ="DSTotal" runat="server" AutoGenerateColumns="False"  AllowPaging="True" AllowSorting="True" 
                            BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" PageSize="5" DataSourceID="DSTotalCompra" Width="780px">
                            <Columns>
                                <asp:BoundField DataField="total" HeaderText="Total" SortExpression="total" ReadOnly="True" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DSTotalCompra" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT SUM(cc.cantidad * p.precio) AS total FROM Carrito_Compra AS cc 
INNER JOIN Producto AS p ON cc.id_producto = p.id_producto"></asp:SqlDataSource>

                        <br />
                         <asp:Button ID ="btbConfirmar" type="submit" class="btn btn-primary" Text ="Confirmar Compra" runat="server" OnClick="btnConfirmar_Click" /> 
                        </div>
                </div>
            </div>
        </div>

    <%--</div>--%>
    <!-- /.container -->

</asp:Content>
