﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Data.Sql;
using System.Threading.Tasks;


namespace BigSkynni
{
    public partial class Carrito_Compra : System.Web.UI.Page
    {
        SqlDataReader rs;
        string consulta;
        string id_cliente;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NuevaUsuario"] != null)
            {


                id_cliente = Session["NuevaUsuario"].ToString();

                DS_Carrito.SelectParameters["id_cliente"].DefaultValue = id_cliente;


            }
            else
            {
                Session["NuevaUsuario"] = null;
                Response.Redirect("Login.aspx");

            }


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            
        }


        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_EventHandler(object sender, EventArgs e)
        {

        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            string total = "";
            int id = 0;
            string consultaHistorial = "";
            try
            {
                string consultaSuma = "SELECT SUM(prod.precio  * cc.cantidad) from Carrito_Compra as cc inner join Producto as prod on prod.id_producto = cc.id_producto where cc.id_cliente = '" + id_cliente + "'";
                rs = connection.result(consultaSuma);
                while (rs.Read())
                {
                    total = Convert.ToString(rs.GetSqlMoney(0));
                }
                rs.Close();


                consulta = "Insert into PedidoVenta_Cabecera values ('" + id_cliente + "', GETDATE() , '" + total + "') Select CAST(scope_identity() AS int)";
               // string idPedidoCabecera = "Select CAST(scope_identity() AS int)";
               // connection.ejecutaComando(consulta);
                rs = connection.result(consulta);
                while(rs.Read())
                {
                    id = rs.GetInt32(0);
                }
                string consultaProductos = "Select id_producto, cantidad from carrito_compra where id_cliente = " + id_cliente;
                rs = connection.result(consultaProductos);
                 while(rs.Read())   {
                    string consultaDetalle = "Insert into PedidoVenta_Detalle values ("+ id + "," + rs.GetInt32(0) + "," + rs.GetInt32(1) + ")";
                    connection.ejecutaComando(consultaDetalle);
                    //Inserto en historial 
                    consultaHistorial = "Insert into Historial_Cliente values(" + id_cliente + ", " + rs.GetInt32(0) + ", GETDATE() ,2)";
                    connection.ejecutaComando(consultaHistorial);

                }
                                
                Response.Redirect("Pedido_Venta.aspx?IdPedido=" + id);
                //Server.Transfer("Pedido_Venta.aspx");
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }
    }
}