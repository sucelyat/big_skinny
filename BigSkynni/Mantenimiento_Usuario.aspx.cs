﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BigSkynni
{
    public partial class Mantenimiento_Usuario : Page
    {
        String consulta = "";
        String id_usuario = "";
        SqlDataReader rs;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NuevaOperario"] != null)
            {


            }
            else
            {
                Session["NuevaOperario"] = null;
                Response.Redirect("Login.aspx");

            }

        }

        public void Limpiar()
        {
            txtNombre.Text = "";
            txtUsuario.Text = "";
            txtPassword.Text = "";
            txtPassword2.Text = "";
            lbl_id.Text = "";
        }

        protected void btnGuardar_Click(object sender, CommandEventArgs e)
        {
            try
            {
                if (txtNombre.Text != "" || txtUsuario.Text != "" || txtPassword.Text != "")
                {
                    if (txtPassword.Text == txtPassword2.Text)
                    {
                        if (lbl_id.Text == "")
                        {
                            consulta = "Insert into usuario values ('" + txtNombre.Text + "','" + txtUsuario.Text + "','" + txtPassword.Text + "', 1)";
                        }else
                            consulta = "Update usuario set nombre ='" + txtNombre.Text + "', usuario = '" + txtUsuario.Text + "', password ='" + txtPassword.Text + "' where id_usuario = '" + lbl_id.Text + "'";
                        connection.ejecutaComando(consulta);
                        Limpiar();
                        tbUsuarios.DataBind();
                    }
                    else
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('La confirmacion de Contraseña no es correcta');</script>");
                }else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Complete los datos solicitados');</script>");
            }
            catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }

        protected void tbUsuarios_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            
        }

        protected void tbUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow rows = tbUsuarios.SelectedRow;
                id_usuario = rows.Cells[1].Text;
                lbl_id.Text = id_usuario;
                consulta = "Select * from Usuario where id_usuario = '" + lbl_id.Text +"'";
                rs = connection.result(consulta);
                while(rs.Read())
                {
                    txtNombre.Text = rs.GetString(1);
                    txtUsuario.Text = rs.GetString(2);
                    txtPassword.Text = rs.GetString(3);
                    txtPassword2.Text = rs.GetString(3);

                }
            }catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            try { 
            if(lbl_id.Text != "")
            {
                    consulta = "Delete usuario where id_usuario = '" + lbl_id.Text + "'";
                    connection.ejecutaComando(consulta);
                    Limpiar();
                    tbUsuarios.DataBind();

            }else
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Seleccione un registro ');</script>");
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }
    }
}