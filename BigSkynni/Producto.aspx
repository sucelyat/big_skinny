﻿<%--<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Producto.aspx.cs" Inherits="BigSkynni.Producto" %>--%>
<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Producto.aspx.cs" Inherits="BigSkynni.Producto" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
            <div class="col-xs-6 col-md-4">
               <h2> <a type="submit" class="btn btn-link" href="/Default.aspx">Regresar</a></small></h2>
<%--                <div class="list-group">
                    <a href="/Default.aspx" class="list-group-item">Regresar</a>
                    </div>--%>
            </div>
    <div class="row">
        <div class="col-md-12">

            <h2><asp:Label ID="lblNombreProducto" runat="server" ></asp:Label></h2>
            <div class="col-md-8">

                <div class="span10">
                    <div class="main-image">
                        
                        <asp:Image ID="url_foto" runat="server" Height="500px" Width="500px"/>                    

                    </div>0
                </div>

            </div>
            <div class="col-md-4">
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <%--<div class="muted pull-left">Product Name</div>--%>
                        <div class="pull-right"><span class="badge badge-warning">Descripcion</span></div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                            <h5><n>Codigo:</n> <asp:Label ID="lblIdProducto" runat="server" ></asp:Label> </h5>
                            <p class="muted"><asp:Label ID="lblDescripcion" runat="server" ></asp:Label></p>

                            <p>
                                </p><dl class="dl-horizontal">
                                  <dt>Precio</dt>
                                      <dd><asp:Label ID="lblPrecio" runat="server" ></asp:Label></dd>
                                <%--  <dd><s><asp:Label ID="lblPrecio" runat="server" ></asp:Label></s></dd>--%>
                                 <%-- <dt>Sale Price</dt>
                                  <dd>$129.99</dd>--%>
                                </dl>
                            <p></p>
                            
                            <%--<form class="form-inline">--%>
                              <div class="control-group">
                                    <label>Cantidad</label>
                                    <%--<input id="name" value="1" placeholder="Quantity" class="input-mini" type="text">--%>
                                  <asp:TextBox ID="txtCantidad" runat="server" Text="1" TextMode="Number" ></asp:TextBox>
                                   <%-- <button type="submit" class="btn btn-danger">Add</button>--%>
                                  <asp:Button ID="txtAdd" runat="server" Enabled="false" CssClass="btn btn-danger" Text ="Agregar"  OnClick="btnAdd_Click" />
                              </div>
                            <%--</form>--%>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</asp:Content>

