﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mantenimiento_Producto.aspx.cs" Inherits="BigSkynni.Mantenimiento_Producto" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Mantenimientos</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">
                    <div class="col-md-12">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1><u>Productos </u></h1>
                        <br>  
                    <label for="Producto">Producto:</label>
                   <asp:TextBox  ID ="txtNombre" runat ="server" class="form-control" placeholder="Nombre" ></asp:TextBox>
                        <label for="precio">Precio:</label>
                        <asp:TextBox  ID ="txtPrecio" runat ="server" class="form-control" placeholder="Precio $." ></asp:TextBox>
                        <label for="desc">Descripcion:</label>
                        <asp:TextBox  ID ="txtDesc" runat ="server" class="form-control" placeholder="Descripcion" ></asp:TextBox>
                        <br />
                            <label for="Tamaño">Tama;o:</label>
                        
                        <asp:DropDownList  ID ="LBTamanio" CssClass="form-control" runat="server" DataSourceID="DSListaTamanio" DataTextField="descripcion" DataValueField="id_tamanio">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList >
                            <asp:SqlDataSource ID="DSListaTamanio" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT * FROM [Tamanio] ORDER BY [descripcion]"></asp:SqlDataSource>
                            <br />
                        <label for="Color">Color:</label>
                           <asp:DropDownList  ID ="LBColor" CssClass="form-control" runat="server" DataSourceID="DSListaColor" DataTextField="descripion" DataValueField="id_color">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                            <asp:SqlDataSource ID="DSListaColor" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_color, descripion FROM Color ORDER BY descripion"></asp:SqlDataSource>
                            <br />
                         <label for="Forma">Forma:</label>
                           <asp:DropDownList  ID ="LBForma" CssClass="form-control" runat="server" DataSourceID="DSListaForma" DataTextField="descripcion" DataValueField="id_forma">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                            <asp:SqlDataSource ID="DSListaForma" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_forma, descripcion FROM Forma ORDER BY descripcion"></asp:SqlDataSource>
                            <br />     
                           <label for="Material">Material:</label>
                           <asp:DropDownList  ID ="LBMaterial" CssClass="form-control" runat="server" DataSourceID="DSListaMaterial" DataTextField="descripcion" DataValueField="id_material">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                            <asp:SqlDataSource ID="DSListaMaterial" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_material, descripcion FROM Material ORDER BY descripcion"></asp:SqlDataSource>
                            <br />       
                        
                            <label for="Genero">Genero:</label>
                           <asp:DropDownList  ID ="LBGenero" CssClass="form-control" runat="server" DataSourceID="DSListaGenero" DataTextField="descripcion" DataValueField="id_genero">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                            <asp:SqlDataSource ID="DSListaGenero" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_genero, descripcion FROM Genero ORDER BY descripcion"></asp:SqlDataSource>
                            <br />      
                        <label for="Imagen">Imagen:</label>
                        <br />  
                        <asp:FileUpload ID="FileUploader" runat="server" OnDataBinding="FileUploader_Clcik" /><br />
                                                <asp:Button ID ="btbImagen" type="submit" class="btn btn-link" Text ="Cargar Imagen" runat="server" OnCommand="btnImagen_Click" /> 
                        <asp:Image ID="imgProd" runat ="server" BorderStyle="Double" Height="100px" ImageAlign="TextTop" ImageUrl="" Width="100px" BorderColor="Black" />      
                        <asp:Label ID="lbl_id" runat="server" Text="" Visible="False"></asp:Label>
                        <br>    
                        <asp:Button ID ="btbGuardar" type="submit" class="btn btn-primary" Text ="Guardar" runat="server" OnCommand="btnGuardar_Click" /> 
                        <asp:Button ID ="btnBorrar" type="submit" class="btn btn-primary" Text ="Borrar" runat="server" OnClick="btnBorrar_Click" /> 
                        </div>
                    <br />

                      <asp:GridView CssClass ="table table-responsive" ID="tbProducto" runat="server" AutoGenerateColumns="False" DataKeyNames="id_producto" DataSourceID="DSDatos" AllowPaging="True" AllowSorting="True" 
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" OnSelectedIndexChanging="tbProducto_SelectedIndexChanging" OnSelectedIndexChanged="tbProducto_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="id_producto" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="id_producto">
                                <FooterStyle ForeColor="White" />
                                </asp:BoundField>
                                <asp:BoundField DataField="nombre" HeaderText="Producto" SortExpression="nombre" />
                                <asp:BoundField DataField="precio" HeaderText="Precio" SortExpression="precio" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DSDatos" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT DISTINCT id_producto, nombre, precio FROM [Producto] ORDER BY [id_producto]">
                             </asp:SqlDataSource>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

</asp:Content>
