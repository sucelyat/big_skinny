﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default_Operario.aspx.cs" Inherits="BigSkynni.Default_Operario" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Tienda Virtual</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="media/imgs/1.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="media/imgs/2.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="media/imgs/3.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <ASP:Repeater id="Catalogo" runat="server">
                        <ItemTemplate>
                         <div class="col-sm-4 col-lg-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<%#DataBinder.Eval(Container.DataItem, "url_foto")%>" alt="">
                                <div class="caption">
                                    <h4 class="pull-right"> <%#String.Format( new System.Globalization.CultureInfo("es-GT"), "{0:C}",DataBinder.Eval(Container.DataItem, "precio"))%> </h4>
                                     <h4><a href="#">
                                        <%#DataBinder.Eval(Container.DataItem, "nombre")%>
                                        <%--<asp:HyperLink class="text" NavigateUrl="<%# "mainframeset.aspx?CatType=" + DataBinder.Eval(Container.DataItem,"Sub_Category_ID")%>" Text="<%#DataBinder.Eval(Container.DataItem, "Sub_Category_Text")%>" runat="server" target="mainFrame" ID="Hyperlink1" NAME="Hyperlink1"/>--%>
                                        </a>
                                    </h4>
                                    <p><%#DataBinder.Eval(Container.DataItem, "descripcion")%></p>
                                </div>
                                <div class="ratings">
                                    <p class="pull-right">15 reviews</p>
                                    <p>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                      </ItemTemplate>
                    </ASP:Repeater>
                  </div>
                </div>
        </div>
    </div>
    <!-- /.container -->

</asp:Content>
