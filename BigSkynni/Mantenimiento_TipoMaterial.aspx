﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mantenimiento_TipoMaterial.aspx.cs" Inherits="BigSkynni.Mantenimiento_TipoMaterial" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <%--<div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>--%>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Mantenimientos</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1><u>Catálogo de Tipo Material </u></h1>
                        <br>  
                    <label for="Tipo_Material">Descripción:</label>
                    <asp:TextBox  ID ="txtDescripcion" runat ="server" class="form-control" placeholder="Tipo Material" ></asp:TextBox>
                        <br>    
                        <asp:Button ID ="btbGuardar" type="submit" class="btn btn-primary" Text ="Guardar" runat="server" OnClick="btnGuardar_Click" /> 

                        <br />
                        <br />
                        <br />
                        <asp:GridView CssClass ="table table-responsive"  ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_material" DataSourceID="DSDatos" AllowPaging="True" AllowSorting="True" 
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" PageSize="5" >
                            <Columns>
                                <asp:CommandField ShowEditButton="True">
                                <ControlStyle ForeColor="#0066FF" />
                                </asp:CommandField>
                                <asp:CommandField ShowDeleteButton="True">
                                <ControlStyle ForeColor="Red" />
                                </asp:CommandField>
                                <asp:BoundField DataField="id_material" HeaderText="Identificador" InsertVisible="False" ReadOnly="True" SortExpression="id_material" Visible="False" />
                                <asp:BoundField DataField="descripcion" HeaderText="Descripcion" SortExpression="descripcion" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DSDatos" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT DISTINCT * FROM [Material] ORDER BY [id_material]" 
                            UpdateCommand="UPDATE Material SET descripcion = @descripcion WHERE (id_material = @id_material)" 
                            DeleteCommand="Delete Material WHERE (id_material = @id_material)">
                            <DeleteParameters>
                                <asp:Parameter Name="id_material" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="descripcion" />
                                <asp:Parameter Name="id_material" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <br />
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

</asp:Content>
