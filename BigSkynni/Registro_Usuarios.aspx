﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registro_Usuarios.aspx.cs" Inherits="BigSkynni.Registro_Usuarios" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Tienda Virtual</p>
            </div>

            <div class="col-md-9">
                <div class="col-md-1">

                </div>
                <div class="col-md-10">
                       <h4 class="form-signin-heading"> Registro de Usuarios </h4>
                       <br />
                       <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblNombre">Nombre:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txtNombre" runat ="server" class="form-control" placeholder="Ejemplo. Juan Perez" ></asp:TextBox>
                           </div>
                       </div>
                        <br />
                        <br />
                       <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblCorreo">Correo:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txtCorreo" runat ="server" class="form-control" placeholder="ejemplo@ejemplo.com" TextMode="Email" ></asp:TextBox>
                            </div>
                       </div>
                       <br />
                       <br />
                       <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblPassword">Password:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="Password" runat ="server" class="form-control" placeholder="Password" TextMode ="Password"></asp:TextBox>
                       
                           </div>
                       </div>
                       <br />
                       <br />
                        <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblsPassword">Confirme:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="Password2" runat ="server" class="form-control" placeholder="Confirme Password" TextMode ="Password"></asp:TextBox>
                            </div>
                       </div>
                       <br />
                       <br />
                       <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblDir">Direccion:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txtDireccion" runat ="server" class="form-control" placeholder="Direccion"></asp:TextBox>     
                            </div>
                       </div>
                       <br />
                       <br />
                       <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lbltel">Telefono:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txttel" runat ="server" class="form-control" placeholder="Telefono"></asp:TextBox>     
                            </div>
                       </div>
                       <br />
                       <br />
                        <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lbltarjeta">Tarjeta:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txtTarjeta" runat ="server" class="form-control" placeholder="Tarjeta" ></asp:TextBox>     
                            </div>
                       </div>
                       <br />
                       <br />

                        <div class="col-md-12">
                        <div class="col-md-10">
                        <asp:RadioButton ID="rdHombre" runat="server" Text ="Hombre" value="Hombre" />
                        <asp:RadioButton ID="rdMujer" runat="server" Text ="Mujer" value="Mujer" />
                        </div>
                       </div>
                       <br />
                       <br />

                        <div class="col-md-12">
                               <div class="col-md-2">
                                   <label for="lblfecha">Fecha Nacimiento:</label>
                               </div>
                           <div class="col-md-10">
                                <asp:TextBox  ID ="txtFecha" runat ="server" class="form-control" placeholder="Fecha Nacimiento" TextMode ="Date" ></asp:TextBox>     
                            </div>
                       </div>
                       <br />
                       <br />

                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                                      <asp:Button CssClass="btn btn-lg btn-primary btn-block" type="submit" ID="btbRegistro" runat ="server" Text ="Registrar" OnClick="btbRegistro_Click"/>
                        </div>
                        <div class="col-md-4"></div>
                       
                </div>
                <div class="col-md-1">

                </div>
               
            </div>

        </div>

    </div>
    <!-- /.container -->

</asp:Content>

