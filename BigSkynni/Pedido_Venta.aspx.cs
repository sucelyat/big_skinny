﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


    namespace BigSkynni
{
    public partial class Pedido_Venta : System.Web.UI.Page
    {
        SqlDataReader rs;
        String consulta;
        String id_Cliente;
        String pedido;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NuevaUsuario"] != null)
            {
                id_Cliente = Session["NuevaUsuario"].ToString();

                DSDatosCliente.SelectParameters["id_cliente"].DefaultValue = id_Cliente;
                pedido = Request.QueryString["IdPedido"];
                SqlDataSource1.SelectParameters["idPedidoCab"].DefaultValue = pedido;

            }
            else
            {
                Session["NuevaUsuario"] = null;
                Response.Redirect("Login.aspx");

            }

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_EventHandler(object sender, EventArgs e)
        {

        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                consulta = @"select client.nombre, client.direccion, client.telefono, client.correo, pdc.id_pedido, pdc.total, pdc.fecha from PedidoVenta_Cabecera as pdc 
                            inner join Cliente as client on client.id_cliente = pdc.id_cliente where pdc.id_cliente = '" + id_Cliente  + "' and pdc.id_pedido = " + pedido;
                rs = connection.result(consulta);
                while (rs.Read())
                {
                    lblNombre.Text = "Nombre: " + rs.GetString(0);
                    lblDireccion.Text = "Direccion: " + rs.GetString(1);
                    lblTelefono.Text = "Tel.: " + rs.GetString(2);
                    lblCorreo.Text = "Correo: " + rs.GetString(3);
                    lblPedido.Text = "No. Pedido: " + Convert.ToString(rs.GetInt32(4));
                    lblTotal.Text = "$. " + Convert.ToString(rs.GetSqlMoney(5));
                    lblSubTotal.Text = "$. " + Convert.ToString(rs.GetSqlMoney(5));
                    lblFecha.Text = "Fecha: " + Convert.ToString(rs.GetDateTime(6));
                }
                panel.Visible = true;
                consulta = "Delete Carrito_Compra where id_cliente = " + id_Cliente;
                connection.ejecutaComando(consulta);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }
    }
}