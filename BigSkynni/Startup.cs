﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BigSkynni.Startup))]
namespace BigSkynni
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
