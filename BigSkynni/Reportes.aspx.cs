﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BigSkynni
{
    public partial class Reportes : Page
    {
        String consulta = "";
        String validacion = "";
        SqlDataReader rs;
        String forma = "";
        String color = "";
        String genero = "";
        String material = "";
        String tamanio = "";
        String fechaIni = "";
        String fechaFin = "";
        String edad = "";
        String nombre = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["NuevaOperario"] != null)
            {
                Limpiar();
            }
            else
            {
                Session["NuevaOperario"] = null;
                Response.Redirect("Login.aspx");

            }
        }

        public void Limpiar()
        {

        }
        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                forma = LBForma.SelectedValue;
                color = LBColor.SelectedValue;
                genero = LBGenero.SelectedValue;
                material = LBMaterial.SelectedValue;
                tamanio = LBTamanio.SelectedValue;
                fechaIni = txtFechaIni.Text;
                fechaFin = txtFechaFin.Text;
                edad = txtEdad.Text;
                nombre = txtCliente.Text;
                if(fechaIni == "")
                {
                    fechaIni = DateTime.Now.ToString("dd/MM/yyyy");
                }
                if (fechaFin == "")
                {
                    fechaFin = DateTime.Now.ToString("dd/MM/yyyy");
                }

                consulta = @"select cl.nombre as Cliente,  (cast(datediff(dd, cl.fecha_nacimiento,GETDATE()) / 365.25 as int)) as Edad,
                            prd.nombre as Producto, hc.fecha, 
                            color.descripion as Color, forma.descripcion as Forma,
                            genero.descripcion as Genero, material.descripcion as Material,
                            tamanio.descripcion as Tamaño,
                            IIF(hc.estado = 2, 'Venta', 'Busqueda') as Tipo   --1 Busqueda , 2 Venta
                            from Historial_Cliente as hc
                            inner join Producto as prd on prd.id_producto = hc.id_producto
                            inner join Cliente as cl on cl.id_cliente = hc.id_cliente
                            inner join Color as color on color.id_color = prd.id_color
                            inner join Forma as forma on forma.id_forma = prd.id_forma
                            inner join Genero as genero on genero.id_genero = prd.id_genero
                            inner join Material as material on material.id_material = prd.id_material
                            inner join Tamanio as tamanio on tamanio.id_tamanio = prd.id_tamanio
                            where hc.fecha between '" + fechaIni + "' and '" + fechaFin +"'";
                if (forma != "")
                {
                    
                    validacion = validacion + " and prd.id_forma = '" + forma + "'";
                }
                if(color != "")
                {
                    validacion = " and prd.id_color = '" + color + "'";
                }
                if (genero != "")
                {
                    validacion = validacion + " and prd.id_genero = '" + genero + "'";
                }
                if(material != "")
                {
                    validacion = validacion + " and prd.id_material = '" + material + "'";
                }
                 if (tamanio != "")
                {
                    validacion = validacion + " and prd.id_tamanio = '" + tamanio + "'";
                }
                if (edad != "")
                {
                    validacion = validacion + " and cl.edad = '" + edad + "'";
                }
                if (nombre != "")
                {
                    validacion = validacion + " and cl.nombre like '%" + nombre + "%'";
                }
                consulta = consulta + validacion;
                rs = connection.result(consulta);
                GridView2.DataSource = rs;
                GridView2.DataBind();
            }catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }


        protected void LBForma_MyListDataBound(object sender, EventArgs e)
        {
            LBForma.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBColor_MyListDataBound(object sender, EventArgs e)
        {
            LBColor.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBGenero_MyListDataBound(object sender, EventArgs e)
        {
            LBGenero.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBMaterial_MyListDataBound(object sender, EventArgs e)
        {
            LBMaterial.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBTamanio_MyListDataBound(object sender, EventArgs e)
        {
            LBTamanio.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }
    }
}