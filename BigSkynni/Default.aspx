﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BigSkynni.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Tienda Virtual<small> -Opciones-</small></p>
                <div class="list-group">
                    <a href="/Busqueda.aspx" class="list-group-item">Busqueda de Productos</a>
                    <a href="/Carrito_Compra.aspx" class="list-group-item">Carrito de Compra</a>
                    <a href="/Pedido_Venta.aspx" class="list-group-item">Compras Realizadas</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="media/imgs/1.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="media/imgs/2.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="media/imgs/3.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <ASP:Repeater id="Catalogo" runat="server">
                        <ItemTemplate>
                         <div class="col-sm-4 col-lg-4 col-md-4">
                            <div class="thumbnail">
                                <a href="/Producto?id_producto= <%#DataBinder.Eval(Container.DataItem, "id")%>">
                                    <img src="<%#DataBinder.Eval(Container.DataItem, "url_foto")%>" alt="">
                                
                                <div class="caption">
                                   <p>  <h4><%#DataBinder.Eval(Container.DataItem, "nombre")%>
                                        <%--<asp:HyperLink class="text" NavigateUrl="<%# "mainframeset.aspx?CatType=" + DataBinder.Eval(Container.DataItem,"Sub_Category_ID")%>" Text="<%#DataBinder.Eval(Container.DataItem, "Sub_Category_Text")%>" runat="server" target="mainFrame" ID="Hyperlink1" NAME="Hyperlink1"/>--%>
                                         </h4>
                                    </p>
                                    </a>
                                    <div class="col-md-12">
                                        <div class="col-sm-9">
                                            <p><%#DataBinder.Eval(Container.DataItem, "descripcion")%></p>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="pull-right"> <%#String.Format( new System.Globalization.CultureInfo("en-US"), "{0:C}",DataBinder.Eval(Container.DataItem, "precio"))%> </h4>
                                        </div>
                                    </div>
                                    <br />
                                    </div>
                                <div class="ratings">
                                    <p class="pull-right">15 reviews</p>
                                    <p>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                      </ItemTemplate>
                    </ASP:Repeater>
                  </div>
                </div>
        </div>
    </div>
    <!-- /.container -->

</asp:Content>
