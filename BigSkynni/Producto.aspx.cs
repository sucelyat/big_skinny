﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigSkynni
{
    public partial class Producto : System.Web.UI.Page
    {
        SqlDataReader rs;
        String consulta = "";
        int idCliente;
        protected void Page_Load(object sender, EventArgs e)
        {
            var usuarioLogeado = Session["NuevaUsuario"];

            if(Session["NuevaUsuario"] != null)
            {
                txtAdd.Enabled = true;
            }

            var idProducto = Request.QueryString["id_producto"];


            string consultaInsert = "Insert into Historial_Cliente values(" + usuarioLogeado + "," + idProducto + ", GETDATE(), 1)";
            connection.ejecutaComando(consultaInsert);

            if (idProducto != null)
            {

                lblNombreProducto.Text = Server.UrlDecode(Request.QueryString["id_producto"].ToString());
                //agregar sql de busqueda por prod

                string strCon = @"
                select  Producto.id_producto id,
	                    Producto.nombre,
	                    Producto.precio,
	                    Tamanio.descripcion tamano,
	                    Color.descripion color,
	                    forma.descripcion forma,
	                    Material.descripcion material,
	                    Producto.descripcion descripcion,
	                    Producto.url_foto
                    from
                    Producto
                    left join Tamanio
                    on
                    Producto.id_tamanio = Tamanio.id_tamanio
                    left join Color
                    on 
                    Producto.id_color = Color.id_color
                    left join Material
                    on 
                    Producto.id_material = Material.id_material
                    left join Forma
                    on
                    Producto.id_forma = Forma.id_forma
            ";
                strCon = strCon + (" where Producto.id_producto = " + idProducto);

                //lblNombreProducto.Text = strCon;
                rs = connection.result(strCon);

                int id = 0;
                string nombre = null;
                string descripcion = null;
                string precio = null;
                string tamano = null;
                string color = null;
                string forma = null;
                string url_fotos = null;
                string material = null;
                

                while (rs.Read())
                {
                    id = rs.GetInt32(0);
                    nombre = rs.GetString(1);
                    precio = Convert.ToString(rs.GetSqlMoney(2));
                    tamano = rs.GetString(3);
                    color = rs.GetString(4);
                    forma = rs.GetString(5);
                    material = rs.GetString(6);
                    descripcion = rs.GetString(7);
                    url_fotos = rs.GetString(8);
                
                }

                lblNombreProducto.Text = nombre;
                lblDescripcion.Text = descripcion;
                lblIdProducto.Text = id.ToString();
                lblPrecio.Text = precio;
                url_foto.ImageUrl = url_fotos;
                

            }
            else
            {
                Response.Redirect("Login.aspx");
                
            }



        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            idCliente = Convert.ToInt32(Session["NuevaUsuario"].ToString());

            try
            {
                consulta = "Insert into Carrito_Compra values ('" + lblIdProducto.Text +"','" + txtCantidad.Text + "','" + idCliente + "')";
                connection.ejecutaComando(consulta);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Producto Agregado al Carrito de Compra ');</script>");
                Response.Redirect("Carrito_Compra.aspx");

            }
            catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }
    }
}