﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Busqueda.aspx.cs" Inherits="BigSkynni.Busqueda" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Opciones</p>
                <div class="list-group">
                    <a href="/Busqueda.aspx" class="list-group-item">Busqueda de Productos</a>
                    <a href="/Carrito_Compra.aspx" class="list-group-item">Carrito de Compra</a>
                    <a href="/Pedido_Venta.aspx" class="list-group-item">Compras Realizadas</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                    </div>

                </div>

                <div class="row">

                        <div class="thumbnail">

                            <div class="x_panel">
                  <div class="x_title">
                    <h2>Búsqueda de Productos <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                        <div class="row">

                        <div class="col-xs-4">
                       <label for="Forma">Forma:</label>
                           <asp:DropDownList  ID ="LBForma" CssClass="form-control" runat="server" DataSourceID="DSListaForma" DataTextField="descripcion" DataValueField="id_forma" ondatabound="LBForma_MyListDataBound">
                               <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaForma" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_forma, descripcion FROM Forma ORDER BY descripcion"></asp:SqlDataSource>
                       </div> 

                                                    <div class="col-xs-4">
                       <label for="Color">Color:</label>
                           <asp:DropDownList  ID ="LBColor" CssClass="form-control" runat="server" DataSourceID="DSListaColor" DataTextField="descripion" DataValueField="id_color" ondatabound="LBColor_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaColor" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_color, descripion FROM Color ORDER BY descripion"></asp:SqlDataSource>
                       </div>

                         <div class="col-xs-4">
                       <label for="genero">Genero:</label>
                           <asp:DropDownList  ID ="LBGenero" CssClass="form-control" runat="server" DataSourceID="DSListaGenero" DataTextField="descripcion" DataValueField="id_genero" ondatabound="LBGenero_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaGenero" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_genero, descripcion FROM Genero ORDER BY descripcion"></asp:SqlDataSource>
                       </div>

                         <div class="col-xs-4">
                       <label for="material">Material:</label>
                           <asp:DropDownList  ID ="LBMaterial" CssClass="form-control" runat="server" DataSourceID="DSListaMaterial" DataTextField="descripcion" DataValueField="id_material" ondatabound="LBMaterial_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaMaterial" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_material, descripcion FROM Material ORDER BY descripcion"></asp:SqlDataSource>
                       </div>

                        <div class="col-xs-4">
                     <label for="tamanio">Tama;o:</label>
                           <asp:DropDownList  ID ="LBTamanio" CssClass="form-control" runat="server" DataSourceID="DSListaTamanio" DataTextField="descripcion" DataValueField="id_tamanio" ondatabound="LBTamanio_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaTamanio" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_tamanio, descripcion FROM Tamanio ORDER BY descripcion"></asp:SqlDataSource>
                     
                    </div>
 </div> 

                         <br />
                         <br />       

                                 <div class="row">
                                <div class="col-xs-4">
                            <label for="precio">Precio </label>
                   <asp:TextBox  ID ="txtPrecio" runat ="server" class="form-control" TextMode ="Number" placeholder="Precio"  ></asp:TextBox>
                            </div>
                                     <div class="col-xs-8">
                            <label for="nombre">Nombre </label>
                   <asp:TextBox  ID ="txtNombre" runat ="server" class="form-control" placeholder="Nombre del Producto"  ></asp:TextBox>
                            </div>
                                 </div>
                                <br />
                                                                   <div class="row">
                                     <div class="col-xs-6">
                                     <asp:Button ID ="btbGenerar" type="submit" class="btn btn-primary" Text ="Buscar" runat="server" OnClick="btnGenerar_Click" /> 
                                         </div></div>
                                <br />

                            </div>
                            </div>
                </div>


        </div>

    </div>
    <!-- /.container -->

    </div>
</asp:Content>
