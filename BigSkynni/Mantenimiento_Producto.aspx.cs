﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace BigSkynni
{
    public partial class Mantenimiento_Producto : Page
    {
        string consulta = "";
        string ruta = "";
        string id_producto = "";
        string url_foto = "";
        SqlDataReader rs;


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["NuevaOperario"] != null)
            {


            }
            else
            {
                Session["NuevaOperario"] = null;
                Response.Redirect("Login.aspx");

            }
            //FileUploader.SaveAs(Server.MapPath("~/ temp"));

        }

        public void Limpiar()
        {
            txtNombre.Text = "";
            txtPrecio.Text = "";
            lbl_id.Text = "";
        }

        protected void btnGuardar_Click(object sender, CommandEventArgs e)
        {
            try
            {
                if (txtNombre.Text != "" || txtPrecio.Text != "")
                {
                    if (lbl_id.Text == "")
                    {
                        consulta = "Insert into producto values ('" + txtNombre.Text + "','" + txtPrecio.Text + "','" + LBTamanio.SelectedValue + "','" + LBColor.SelectedValue + "','" + LBForma.SelectedValue + "','" + LBMaterial.SelectedValue + "','" + LBGenero.SelectedValue + "','" + imgProd.ImageUrl + "','" + txtDesc.Text + "')";
                    }
                    else
                    {
                        consulta = "Update producto set nombre ='" + txtNombre.Text + "', precio = '" + txtPrecio.Text + "', id_tamanio ='" + LBTamanio.SelectedValue + "', id_color = '" + LBColor.SelectedValue + "', id_forma = '" + LBForma.SelectedValue + "', id_material = '" + LBMaterial.SelectedValue + "', id_genero = '" + LBGenero.SelectedValue + "', url_foto = '" + imgProd.ImageUrl + "', descripcion = '" + txtDesc.Text + "' where id_producto = '" + lbl_id.Text + "'";
                    }
                    
                    connection.ejecutaComando(consulta);
                    Limpiar();
                    tbProducto.DataBind();
                }else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Complete los datos solicitados');</script>");
            }
            catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }

        protected void btnImagen_Click(object sender, CommandEventArgs e)
        {
            if(FileUploader.HasFile)
                {
                string nombreArchivo = Path.GetFileName(FileUploader.PostedFile.FileName);
                string rutaArchivo = "/media/imgs/subidas/";
                FileUploader.PostedFile.SaveAs(Server.MapPath(rutaArchivo) +nombreArchivo);
                url_foto = rutaArchivo + nombreArchivo;

                imgProd.ImageUrl = url_foto;
                
            }
        }

        protected void FileUploader_Clcik(object sender, EventArgs e)
        {
           // string fullPath = Path.Combine(Server.MapPath("~/temp"), file.FileName);
            imgProd.ImageUrl = Server.MapPath("~/temp");
          
        }

        protected void tbProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow rows = tbProducto.SelectedRow;
                id_producto = rows.Cells[1].Text;
                lbl_id.Text = id_producto;
                consulta = "Select * from Producto where id_producto = '" + lbl_id.Text + "'";
                rs = connection.result(consulta);
                while (rs.Read())
                {
                    txtNombre.Text = rs.GetString(1);
                    txtPrecio.Text = Convert.ToString(rs.GetSqlMoney(2));
                    if (rs.GetString(9) != null) txtDesc.Text = rs.GetString(9);
                    LBTamanio.SelectedValue = rs.GetInt32(3).ToString();
                    LBColor.SelectedValue = rs.GetInt32(4).ToString();
                    LBForma.SelectedValue = rs.GetInt32(5).ToString();
                    LBMaterial.SelectedValue = rs.GetInt32(6).ToString();
                    LBGenero.SelectedValue = rs.GetInt32(7).ToString();
                    imgProd.ImageUrl = rs.GetString(9).ToString();

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }

        protected void tbProducto_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbl_id.Text != "")
                {
                    consulta = "Delete producto where id_producto = '" + lbl_id.Text + "'";
                    connection.ejecutaComando(consulta);
                    Limpiar();
                    tbProducto.DataBind();

                }
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Seleccione un registro ');</script>");
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }
    }
}