﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace BigSkynni
{
    public partial class Busqueda : Page
    {
        SqlDataReader rs;
        String forma = "";
        String color = "";
        String genero = "";
        String material = "";
        String tamanio = "";
        String precio = "";
        String nombre = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                forma = LBForma.SelectedValue;
                color = LBColor.SelectedValue;
                genero = LBGenero.SelectedValue;
                material = LBMaterial.SelectedValue;
                tamanio = LBTamanio.SelectedValue;
                precio = txtPrecio.Text;
                nombre = txtNombre.Text;

                Response.Redirect("Resultado_Busqueda.aspx?Forma=" + forma + "&Color=" + color + "&Genero=" + genero + "&Material=" + material + "&Tamanio=" + tamanio + "&Precio=" + precio + "&Nombre="+ nombre);

            }catch(Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Error: ');</script>");
            }
        }


        protected void LBForma_MyListDataBound(object sender, EventArgs e)
        {
            LBForma.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBColor_MyListDataBound(object sender, EventArgs e)
        {
            LBColor.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBGenero_MyListDataBound(object sender, EventArgs e)
        {
            LBGenero.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBMaterial_MyListDataBound(object sender, EventArgs e)
        {
            LBMaterial.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }

        protected void LBTamanio_MyListDataBound(object sender, EventArgs e)
        {
            LBTamanio.Items.Insert(0, new ListItem("- Seleccionar -", ""));
        }
    }
}