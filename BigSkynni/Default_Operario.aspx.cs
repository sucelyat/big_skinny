﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigSkynni
{
    public partial class Default_Operario : Page
    {
        SqlDataReader rs;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["NuevaOperario"]!= null)
            {
                string strCon = @"
                select top 9
	                    Producto.id_producto id,
	                    Producto.nombre,
	                    Producto.precio,
	                    Tamanio.descripcion tamano,
	                    Color.descripion color,
	                    forma.descripcion forma,
	                    Material.descripcion material,
	                    Producto.descripcion descripcion,
	                    Producto.url_foto
                    from
                    Producto
                    left join Tamanio
                    on
                    Producto.id_tamanio = Tamanio.id_tamanio
                    left join Color
                    on 
                    Producto.id_color = Color.id_color
                    left join Material
                    on 
                    Producto.id_material = Material.id_material
                    left join Forma
                    on
                    Producto.id_forma = Forma.id_forma
            ";
                rs = connection.result(strCon);

                Catalogo.DataSource = rs;
                Catalogo.DataBind();


            }
            else
            {
                Session["NuevaOperario"] = null;
                Response.Redirect("Login.aspx");

            }

            
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {

        }
    }
}