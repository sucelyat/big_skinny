﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigSkynni
{
    public partial class Default : Page
    {
        SqlDataReader rs;
        protected void Page_Load(object sender, EventArgs e)
        {
            var cliente = Request.QueryString["Cliente"];
            string strCon = @"
                select top 9
	                    Producto.id_producto id,
	                    Producto.nombre,
	                    Producto.precio,
	                    Tamanio.descripcion tamano,
	                    Color.descripion color,
	                    forma.descripcion forma,
	                    Material.descripcion material,
	                    Producto.descripcion descripcion,
	                    Producto.url_foto
                    from
                    Producto
                    left join Tamanio
                    on
                    Producto.id_tamanio = Tamanio.id_tamanio
                    left join Color
                    on 
                    Producto.id_color = Color.id_color
                    left join Material
                    on 
                    Producto.id_material = Material.id_material
                    left join Forma
                    on
                    Producto.id_forma = Forma.id_forma
                    ORDER BY NEWID()
            ";
            rs = connection.result(strCon);

            Catalogo.DataSource = rs;
            Catalogo.DataBind();
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {

        }
    }
}