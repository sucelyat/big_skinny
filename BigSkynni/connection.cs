﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;

namespace BigSkynni
{
    class connection
    {
        private static Stack<SqlConnection> pila = new Stack<SqlConnection>();

        public static SqlConnection conect()
        {
            // string MyConString = "data source = " + Properties.Settings.Default.servidor + "; initial catalog = " + Properties.Settings.Default.bd + "; Persist Security Info=True; user id = " + Properties.Settings.Default.usuario + "; password = " + Properties.Settings.Default.pass + "";

            string MyConString = @"data source = .\SQLEXPRESS;Initial Catalog=BIG_SKINNY;Persist Security Info =True; User id = AdminBS; Password = Secret0 ";

                 SqlConnection cnn = new SqlConnection(MyConString);
            cnn.Open();
            pila.Push(cnn);
            return cnn;
        }

        public static SqlDataReader result(String consulta)
        {
            SqlConnection cnn = conect();
            SqlCommand command = cnn.CreateCommand();
            SqlDataReader rs;
            command.CommandText = consulta;
            rs = command.ExecuteReader();
            return rs;

        }

        public static void cerrarConexion()
        {
            while (pila.Count > 0)
            {
                pila.Pop().Close();
            }
            conect().Close();
        }


        public static void ejecutaComando(String codigo)
        {
            SqlConnection cnn = conect();
            SqlCommand comando = cnn.CreateCommand();
            comando.CommandText = codigo;
            comando.ExecuteNonQuery();
            cnn.Close();
        }

        public static void ejecutaScalar(String codigo)
        {
            SqlConnection cnn = conect();
            SqlCommand comando = cnn.CreateCommand();
            comando.CommandText = codigo;
            comando.ExecuteScalar();
            cnn.Close();
        }
    }
}