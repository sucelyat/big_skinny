﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reportes.aspx.cs" Inherits="BigSkynni.Reportes" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Mantenimientos</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                    </div>

                </div>

                <div class="row">

                        <div class="thumbnail">

                            <div class="x_panel">
                  <div class="x_title">
                    <h2>Analisis de Preferencias de Clientes <small>Reportes</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                        <div class="row">

                        <div class="col-xs-4">
                       <label for="Forma">Forma:</label>
                           <asp:DropDownList  ID ="LBForma" CssClass="form-control" runat="server" DataSourceID="DSListaForma" DataTextField="descripcion" DataValueField="id_forma" ondatabound="LBForma_MyListDataBound">
                               <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaForma" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_forma, descripcion FROM Forma ORDER BY descripcion"></asp:SqlDataSource>
                       </div> 

                                                    <div class="col-xs-4">
                       <label for="Color">Color:</label>
                           <asp:DropDownList  ID ="LBColor" CssClass="form-control" runat="server" DataSourceID="DSListaColor" DataTextField="descripion" DataValueField="id_color" ondatabound="LBColor_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaColor" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_color, descripion FROM Color ORDER BY descripion"></asp:SqlDataSource>
                       </div>

                         <div class="col-xs-4">
                       <label for="genero">Genero:</label>
                           <asp:DropDownList  ID ="LBGenero" CssClass="form-control" runat="server" DataSourceID="DSListaGenero" DataTextField="descripcion" DataValueField="id_genero" ondatabound="LBGenero_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaGenero" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_genero, descripcion FROM Genero ORDER BY descripcion"></asp:SqlDataSource>
                       </div>

                         <div class="col-xs-4">
                       <label for="material">Material:</label>
                           <asp:DropDownList  ID ="LBMaterial" CssClass="form-control" runat="server" DataSourceID="DSListaMaterial" DataTextField="descripcion" DataValueField="id_material" ondatabound="LBMaterial_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaMaterial" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_material, descripcion FROM Material ORDER BY descripcion"></asp:SqlDataSource>
                       </div>

                        <div class="col-xs-4">
                     <label for="tamanio">Tama;o:</label>
                           <asp:DropDownList  ID ="LBTamanio" CssClass="form-control" runat="server" DataSourceID="DSListaTamanio" DataTextField="descripcion" DataValueField="id_tamanio" ondatabound="LBTamanio_MyListDataBound">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>

                         <asp:SqlDataSource ID="DSListaTamanio" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT id_tamanio, descripcion FROM Tamanio ORDER BY descripcion"></asp:SqlDataSource>
                     
                    </div>
 </div> 

                         <br />
                         <br />       
                        <div class="row">

                        <div class="col-xs-4">
                             <label for="Inicio">Del:</label>
                   <asp:TextBox  ID ="txtFechaIni" runat ="server" class="form-control" TextMode ="Date" ></asp:TextBox>
                            </div>
                            <div class="col-xs-4">
                            <label for="Fin">Al:</label>
                   <asp:TextBox  ID ="txtFechaFin" runat ="server" class="form-control" TextMode ="Date" ></asp:TextBox>
                            </div></div>

                                 <div class="row">
                                <div class="col-xs-4">
                            <label for="edad">Edad </label>
                   <asp:TextBox  ID ="txtEdad" runat ="server" class="form-control" TextMode = "Number" placeholder="Edad"  ></asp:TextBox>
                            </div>
                                     <div class="col-xs-8">
                            <label for="cliente">Cliente </label>
                   <asp:TextBox  ID ="txtCliente" runat ="server" class="form-control" placeholder="Nombre del Cliente"  ></asp:TextBox>
                            </div>
                                 </div>
                                <br />
                                                                   <div class="row">
                                     <div class="col-xs-6">
                                     <asp:Button ID ="btbGenerar" type="submit" class="btn btn-primary" Text ="Generar" runat="server" OnClick="btnGenerar_Click" /> 
                                         </div></div>
                                <br />
                  <div class="x_content">

                        <tbody>

                        <asp:GridView ID="GridView2" runat="server" CssClass ="table table-responsive"
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" PageSize="15" >
                        </asp:GridView>

</tbody>
                </div></div>
                            </div>
                </div>


        </div>

    </div>
    <!-- /.container -->

    </div>
</asp:Content>
