﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigSkynni
{
    public partial class Resultado_Busqueda : System.Web.UI.Page
    {
        string validacion = "";
        SqlDataReader rs;
        string forma = "";
        string color = "";
        string genero = "";
        string material = "";
        string tamanio = "";
        string PrecioIni = "";
        string PrecioFin = "";
        string Nombre = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
                forma =  Request.QueryString["Forma"];
                color = Request.QueryString["Color"];
                genero = Request.QueryString["Genero"];
                material = Request.QueryString["Material"];
                tamanio = Request.QueryString["Tamanio"];
                PrecioIni = Request.QueryString["precioI"];
                PrecioFin = Request.QueryString["precioF"];
                Nombre = Request.QueryString["nombre"];

            //Forma=,Color=,Genero=,Material=,Tamanio=,Precio=,Nombre=
            //agregar sql de busqueda por prod

            string strCon = @"
                select  Producto.id_producto id,
	                    Producto.nombre,
	                    Producto.precio,
	                    Tamanio.descripcion tamano,
	                    Color.descripion color,
	                    forma.descripcion forma,
	                    Material.descripcion material,
	                    Producto.descripcion descripcion,
	                    Producto.url_foto
                    from
                    Producto
                    left join Tamanio
                    on
                    Producto.id_tamanio = Tamanio.id_tamanio
                    left join Color
                    on 
                    Producto.id_color = Color.id_color
                    left join Material
                    on 
                    Producto.id_material = Material.id_material
                    left join Forma
                    on
                    Producto.id_forma = Forma.id_forma
					where 
					Producto.id_producto is not null
            ";
            if (forma != "")
            {
                validacion = " and Producto.id_forma = '" + forma + "'";
            }
            if (color != "")
            {
                validacion = validacion + " and Producto.id_color = '" + color + "'";
            }
            if (genero != "")
            {
                validacion = validacion + " and Producto.id_genero = '" + genero + "'";
            }
            if (material != "")
            {
                validacion = validacion + " and Producto.id_material = '" + material + "'";
            }
            if (tamanio != "")
            {
                validacion = validacion + " and Producto.id_tamanio = '" + tamanio + "'";
            }

            //if (PrecioIni != "" || PrecioFin != "")
            //{
            //    var precioInicial = Convert.ToInt32(PrecioIni);
            //    var precioFinal = Convert.ToInt32(PrecioFin);

            //    if (precioInicial < precioFinal)
            //    {
            //        validacion = validacion + "AND Producto.precio between 200 and 500";

            //    }
            //    else
            //    {

                    
            //    }
            //}
            

            strCon = strCon + validacion;

            rs = connection.result(strCon);

            if (rs.HasRows){

                Catalogo.DataSource = rs;
                Catalogo.DataBind();

            }
            else
            {

                Resultado_fallido.Text = "No se encontraron resultados con estos parametros";
            }
            
            
        }
    }
}