﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pedido_Venta.aspx.cs" Inherits="BigSkynni.Pedido_Venta" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Opciones</p>
                <div class="list-group">
                    <a href="/Busqueda.aspx" class="list-group-item">Busqueda de Productos</a>
                    <a href="/Carrito_Compra.aspx" class="list-group-item">Carrito de Compra</a>
                    <a href="/Pedido_Venta.aspx" class="list-group-item">Compras Realizadas</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1><u>Confirmacion de Compra</u></h1>
                        <p>
                            <asp:GridView ID="GridView1" CssClass ="table table-responsive" runat="server" AutoGenerateColumns="False" DataKeyNames="id_cliente" DataSourceID="DSDatosCliente">
                                <Columns>
                                    <asp:CommandField EditText="Actualizar" ShowEditButton="True" />
                                    <asp:BoundField DataField="id_cliente" HeaderText="id_cliente" SortExpression="id_cliente" InsertVisible="False" ReadOnly="True" Visible="False" />
                                    <asp:BoundField DataField="nombre" HeaderText="Nombre" SortExpression="nombre" />
                                    <asp:BoundField DataField="correo" HeaderText="Correo" SortExpression="correo" />
                                    <asp:BoundField DataField="telefono" HeaderText="Telefono" SortExpression="telefono" />
                                    <asp:BoundField DataField="direccion" HeaderText="Dirección" SortExpression="direccion" />
                                    <asp:BoundField DataField="tarjeta" HeaderText="Tarjeta" SortExpression="tarjeta" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="DSDatosCliente" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT [id_cliente], [nombre], [correo], [telefono], [direccion], [tarjeta] FROM [Cliente] WHERE ([id_cliente] = @id_cliente)" UpdateCommand="UPDATE Cliente SET nombre = @nombre, correo = @correo, telefono = @telefono, direccion = @direccion, tarjeta = @tarjeta WHERE (id_cliente = @id_cliente)">
                                <SelectParameters>
                                    <asp:Parameter Name="id_cliente" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="nombre" />
                                    <asp:Parameter Name="correo" />
                                    <asp:Parameter Name="telefono" />
                                    <asp:Parameter Name="direccion" />
                                    <asp:Parameter Name="tarjeta" />
                                    <asp:Parameter Name="id_cliente" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </p>
                        <asp:Button ID ="btbConfirmar" type="submit" class="btn btn-primary" Text ="Confirmar Datos" runat="server" OnClick="btnConfirmar_Click" /> 
                        <br />
                        <br />
                        <br />
                        </div>

                    <div class="row">
                    <div class="col-md-12 col-md-offset-1">
                <asp:Panel ID ="panel" runat="server" Visible ="false">
                 <h1><u>Pedido de Venta</u></h1>
            <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                         <h4>Cliente</h4> 
                          <address>
                              <asp:Label runat="server" ID="lblNombre" />
                              <br />
                              <asp:Label runat="server" ID="lblDireccion" />
                              <br />
                              <asp:Label runat="server" ID="lblTelefono" />
                              <br />
                              <asp:Label runat="server" ID="lblCorreo" />
                              <br />
                              <asp:Label runat="server" ID="lblPedido" />
                              <br />
                              <asp:Label runat="server" ID="lblFecha" Text="00.00.0000" />
                              <br />
                                      </address>
                        </div>
                      </div>
                        <asp:GridView ID="GridView2" runat="server" CssClass ="table table-striped" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                            <Columns>
                                <asp:BoundField DataField="nombre" HeaderText="Producto" SortExpression="nombre" />
                                <asp:BoundField DataField="precio" HeaderText="Precio" SortExpression="precio" />
                                <asp:BoundField DataField="cantidad" HeaderText="Cantidad" SortExpression="cantidad" />
                                <asp:BoundField DataField="Total" HeaderText="Total" ReadOnly="True" SortExpression="Total" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT pr.nombre, pr.precio, pd.cantidad, (pd.cantidad * pr.precio) as Total FROM PedidoVenta_Detalle AS pd INNER JOIN Producto AS pr ON pr.id_producto = pd.id_producto
where pd.id_PedidoCab = @idPedidoCab">
                            <SelectParameters>
                                <asp:Parameter Name="idPedidoCab" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <br />
                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                            <asp:Table ID="tbDetalle" runat="server" Width="100%">
<asp:TableRow ID="fila">
<asp:TableCell ID="cantidad"></asp:TableCell>
<asp:TableCell ID="descripcion"></asp:TableCell>
</asp:TableRow>
</asp:Table>
</div>
                          </div>
                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Tarjetas de Credito:</p>
                          <img src="media/imgs/visa.png" alt="Visa">
                          <img src="media/imgs/mastercard.png" alt="Mastercard">
                          <img src="media/imgs/american-express.png" alt="American Express">
                          <img src="media/imgs/paypal2.png" alt="Paypal">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Total</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td><asp:Label runat="server" ID="lblSubTotal" /></td>
                                </tr>
                                <tr>
                                  <th>Total:</th>
                                  <td><asp:Label runat="server" ID="lblTotal" /></td>
                                </tr>
                                  <tr>
                                      <th></th>
                                      <td> <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Imprimir</button></td>
                                  </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
<%--                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
                        </div>
                      </div>--%>
                    </section>
                  </div>
                </div>
                                              </asp:Panel>
                        </div>
                        </div>
        </div>

    </div>

    
                </div>

            </div>

    <!-- /.container -->

</asp:Content>
