﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mantenimiento_Usuario.aspx.cs" Inherits="BigSkynni.Mantenimiento_Usuario" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <%--<div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>--%>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Mantenimientos</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">
                    <div class="col-md-12">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1><u>Usuarios </u></h1>
                        <br>  
                    <label for="Tamaño">Nombre:</label>
                   <asp:TextBox  ID ="txtNombre" runat ="server" class="form-control" placeholder="Nombre" ></asp:TextBox>
                        <label for="Tamaño">Usuario:</label>
                        <asp:TextBox  ID ="txtUsuario" runat ="server" class="form-control" placeholder="Usuario" ></asp:TextBox>
                        <label for="Tamaño">Password:</label>
                        <asp:TextBox  ID ="txtPassword" runat ="server" class="form-control" placeholder="Password" TextMode ="Password"></asp:TextBox>
                        <label for="Tamaño">Confirmar Password:</label>
                        <asp:TextBox  ID ="txtPassword2" runat ="server" class="form-control" placeholder="Confirmar Password" TextMode ="Password"></asp:TextBox>

                        <br>    
                        <asp:Button ID ="btbGuardar" type="submit" class="btn btn-primary" Text ="Guardar" runat="server" OnCommand="btnGuardar_Click" /> 
                        <asp:Button ID ="btnBorrar" type="submit" class="btn btn-primary" Text ="Borrar" runat="server" OnClick="btnBorrar_Click" /> 

                        <br />
                        <asp:Label ID="lbl_id" runat="server" Text="Label" Visible="False"></asp:Label>
                        <br />
                        <br />
                        <asp:GridView CssClass ="table table-responsive" ID="tbUsuarios" runat="server" AutoGenerateColumns="False" DataKeyNames="id_usuario" DataSourceID="DSDatos" AllowPaging="True" AllowSorting="True" 
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" PageSize="5" OnSelectedIndexChanging="tbUsuarios_SelectedIndexChanging" OnSelectedIndexChanged="tbUsuarios_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                                <asp:BoundField DataField="id_usuario" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="id_usuario">
                                <FooterStyle ForeColor="White" />
                                </asp:BoundField>
                                <asp:BoundField DataField="usuario" HeaderText="Usuario" SortExpression="usuario" />
                                <asp:BoundField DataField="nombre" HeaderText="Nombre" SortExpression="nombre" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DSDatos" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT DISTINCT id_usuario, usuario, nombre FROM [Usuario] ORDER BY [usuario]">
                             </asp:SqlDataSource>
                </div>

            </div>

        </div>
                                </div>

    </div>
    <!-- /.container -->

</asp:Content>
