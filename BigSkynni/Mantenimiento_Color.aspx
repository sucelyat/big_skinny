﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mantenimiento_Color.aspx.cs" Inherits="BigSkynni.Mantenimiento_Color" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Mantenimientos</p>
                <div class="list-group">
                    <a href="/Mantenimiento_Tamanio.aspx" class="list-group-item">Catálogo de Tamaño</a>
                    <a href="/Mantenimiento_Color.aspx" class="list-group-item">Catálogo de Color</a>
                    <a href="/Mantenimiento_Forma.aspx" class="list-group-item">Catálogo de Forma</a>
                    <a href="/Mantenimiento_Genero.aspx" class="list-group-item">Catálogo de Genero</a>
                    <a href="/Mantenimiento_TipoMaterial.aspx" class="list-group-item">Catálogo de Tipo Material</a>
                    <a href="/Mantenimiento_Producto.aspx" class="list-group-item">Catálogo de Producto</a>
                    <a href="/Mantenimiento_Usuario.aspx" class="list-group-item">Usuarios</a>
                    <a href="/Reportes.aspx" class="list-group-item">Reportes</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1><u>Catálogo de Color </u></h1>
                        <br>  
                    <label for="Color">Descripción:</label>
                    <asp:TextBox  ID ="txtDescripcion" runat ="server" class="form-control" placeholder="Color" ></asp:TextBox>
                        <br>    
                        <asp:Button ID ="btbGuardar" type="submit" class="btn btn-primary" Text ="Guardar" runat="server" OnClick="btnGuardar_Click" /> 
                        <br />
                        <br />
                        <br />
                        <asp:GridView CssClass ="table table-responsive" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id_color" DataSourceID="DSDatos" AllowPaging="True" AllowSorting="True" 
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" PageSize="5">
                            <Columns>
                                <asp:CommandField ShowEditButton="True">
                                <ControlStyle ForeColor="#0066FF" />
                                </asp:CommandField>
                                <asp:CommandField ShowDeleteButton="True">
                                <ControlStyle ForeColor="Red" />
                                </asp:CommandField>
                                <asp:BoundField DataField="id_color" HeaderText="id_color" InsertVisible="False" ReadOnly="True" SortExpression="id_color" Visible="False"/>
                                <asp:BoundField DataField="descripion" HeaderText="Descripcion" SortExpression="descripion" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="DSDatos" runat="server" ConnectionString="<%$ ConnectionStrings:BIG_SKINNYConnectionString %>" SelectCommand="SELECT DISTINCT * FROM [Color] ORDER BY [id_color]" UpdateCommand="UPDATE Color SET descripion = @descripion WHERE (id_color = @id_color)" DeleteCommand="Delete Color WHERE (id_color = @id_color)">
                            <DeleteParameters>
                                <asp:Parameter Name="id_color" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="descripion" />
                                <asp:Parameter Name="id_color" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <br />
                        </div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

</asp:Content>
