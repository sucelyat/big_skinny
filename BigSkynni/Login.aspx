﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BigSkynni.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Tienda Virtual</p>
            </div>

            <div class="col-md-9">
                <div class="col-md-2">

                </div>
                <div class="col-md-6">

                        <h4 class="form-signin-heading"> Iniciar Sesion </h4>
                        <label for="inputEmail" class="sr-only">Correo</label>
                        <asp:TextBox id="txtusuarios"  runat ="server" CssClass="form-control" placeholder="Usuario"></asp:TextBox>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <asp:TextBox id="txtPasswords"  runat ="server" CssClass="form-control" placeholder="Password" TextMode ="Password"></asp:TextBox>
                        <div class="checkbox">
                          <label>
                            <input value="remember-me" type="checkbox"> Recordarme
                          </label>
                        </div>
                        <asp:button runat ="server" CssClass="btn btn-lg btn-primary btn-block" type="submit" Text ="Iniciar Sesion" ID ="btnIngresar" OnClick="btnIngresar_Click"></asp:button>

                </div>
                <div class="col-md-1">

                </div>
               
            </div>

        </div>

    </div>
    <!-- /.container -->

</asp:Content>

